import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Arrays
        Scanner indexSearch = new Scanner(System.in);

        String[] fruits = {"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println("Fruits in stock: " + Arrays.toString(fruits));

        System.out.println("Which fruit do you want to get the index of?");
        String Searcher = indexSearch.nextLine();
        System.out.println(String.format("The index of " + Searcher + " is: " + Arrays.binarySearch(fruits, Searcher)));

        // ArrayList
        ArrayList<String> friends = new ArrayList<>();

        // adding of Friends
        friends.add("Tee Jae");
        friends.add("Judy");
        friends.add("Allan");
        friends.add("Alvin");
        System.out.println("My friends are: " + friends);


        // HashMap
        HashMap<String, Integer>  items = new HashMap<>();

        // adding of items
        items.put("toothpaste", 30);
        items.put("toothbrush", 20);
        items.put("soap", 50);

        System.out.println("Our current inventory consists of: " + items);

    }
}