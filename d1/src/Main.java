import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Operators in Java

        // Operators:
            //

        // Control Structures in Java
            /* if Statements allow us to manipulate the flow of the code depending on the evaluation logic

            Syntax:
                if(condition) {

                }
            */

        int num1 = 15;
        if (num1 % 5 == 0) {
            System.out.println(num1 + " is divisible by 5.");
        }

        // else statement will allow us to run a task or code if the if condition fails or receives falsy value.
        int num2 = 36;
        if (num2 % 5 == 0) {
            System.out.println(num2 + " is divisible by 5.");
        } else {
            System.out.println(num2 + " is not divisible by 5.");
        }

        // Mini-Activity:
            // Create/Instantiate a new Scanner object from the Scanner class and it as numberScanner.
            // Ask the user for an interger and store the input in a variable.
            // Add an if-else statement:
                // IF the number is even, show a message: <number> is even!
                // ELSE, show the following message: <number> is odd!

        Scanner userInput = new Scanner(System.in);

        System.out.println("Enter a Number: ");
        int num = userInput.nextInt();

        if (num%2 == 0) {
            System.out.println(num + " is even.");
        } else {
            System.out.println(num + " is odd.");
        }


        // Switch

        System.out.println("Enter a number from 1-4 to see one of the four directions.");
        int directionValue = userInput.nextInt();

        switch (directionValue) {

            case 1:
                System.out.println("North");
                break; // to break process of the code block once it finishes.
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Out of Range");
        }

        // Arrays
            // Arrays are objects that contain a fixed/limited number of values of a single data type.
            // Unlike in other languages like JS, the lingth of Java arrays are established when the array is crated.
            // Syntax:
                // datatype[] identifier = new dataType[<numberOfElements>];
                // datatype[] identifier = {elementA, elementB,...};

        String[] newArray = new String[3];
        newArray[0] = "Clark";
        newArray[1] = "Bruce";
        newArray[2] = "Diana";
        // newArray[1] = 'a'; --- cannot reassign a different datatype
        // newArray[3] = "Barry"; --- error - out of bounds. the length of a Java array is set upon Creation.

        System.out.println(newArray);
            // OUTPUT: [Ljava.lang.String;@3c756e4d
            // we cannot print an array.   ^ this is the "memory address" or the location of the array within the memory
            // We use the "arrays class" to access methods to manipulate and access our array.
            // toString() is used to show the values of the array as a string in the terminal
        System.out.println(Arrays.toString(newArray));

        // Array Methods
            // .toString() Method -> retrive the actual value of the array as a string.

            // Sort Method
            Arrays.sort(newArray);
            System.out.println("Result of Arrays.sort()");
            System.out.println(Arrays.toString(newArray));

            Integer[] intArr = new Integer[3];
            intArr[0] = 54;
            intArr[1] = 12;
            intArr[2] = 67;
        System.out.println("Initial order of the intArr:");
        System.out.println(Arrays.toString(intArr));

        Arrays.sort(intArr);
        System.out.println("Order of items in intArr after sorting:");
        System.out.println(Arrays.toString(intArr));

        // binarySearch() Method - allows us to pass an argument/item to search for with our array. binarySearch() will then return the index number of the found element.
        String searchTerm = "Bruce"; // You may also use the "Scanner" to have input search
        int result = Arrays.binarySearch(newArray, searchTerm);
        System.out.println(String.format("The index of %s is: %s", searchTerm, result));

        /*
            ArrayList
                - Array List are resizable arrays that function similarly to how array work in other languages like JS
                - Using the new keyword in creating an ArrayList does not require the datatype of the array list to be defined to avoid repetition.
                - Syntax:
                        ArrayList<dataType> identifier = new ArrayList<>();
        */

        ArrayList<String> students = new ArrayList<>();

        // ArrayList Methods
        // arrayListName.add(<itemToAdd>) -> this adds elements in our array List
        students.add("Paul");
        students.add("John");
        System.out.println(students);

        // arrayListName.get(index) -> allows us to retrieve items from our array list using its index.
        System.out.println(students.get(1));

        // arrayListName.set(index, value) -> allows us to update an item by its index.
        students.set(0, "George");
        System.out.println(students);

        // arrayListName.remove(index) -> allows us to remove an item by the arrayList based on its index.
        students.remove(1);
        System.out.println(students);
        students.add("James");
        students.add("Wade");
        System.out.println(students);
        students.remove(1); // we can remove a specific element using its index
        System.out.println(students);

        // arrayListName.clear() -> clears out items in the arrayList
        students.clear();
        System.out.println(students);

        // arrayListName.size() -> allows us to get the length of arrayList
        System.out.println(students.size());

        // Arrays with initialized values:
        double[] doubleArray = {76.54, 80.02, 85.54, 79.77};
        System.out.println(Arrays.toString(doubleArray));
        // doubleArray[4] = 93.22; -> still cannot add additional element

        // ArrayList with initialized Values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Junie", "Cong"));
        System.out.println(employees);
        //adding an element to the array
        employees.add("Red");
        System.out.println(employees);

        // HashMaps
            // Most object in Java are defined as Classes that contain a proper set of properties and methods. However, there might be cases where this is not appropriate for your use-case or you may simply want store a collection of data that in a key-value pair
            // In java, "keys" are also referred as "fields".
            // This offers flexibility when storing a collection of data.
                // Syntax
                    /*
                        HashMap<fieldDataType, valueDataType> identifier = new Hashmap<>();
                    */
        HashMap<String, String> userRoles = new HashMap<>();

        // Add new fields in the HashMap:
            // hashMapName.put(<item>);
        userRoles.put("Anna", "Admin");
        userRoles.put("Alice", "User");
        System.out.println(userRoles);

        userRoles.put("Alice", "Teacher");
        System.out.println(userRoles);

        userRoles.put("Dennis", "Admin");
        System.out.println(userRoles);

        // retrieve values by fields
        // hashMapName.get("field");
        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Dennis"));
        System.out.println(userRoles.get("alice")); // -> this is case sensitive: output: NULL

        // remove an element
        // hashMapName.remove("field");
        userRoles.remove("Dennis");
        System.out.println(userRoles);

        // retrieve hashMap keys
        // hashMapName.keySet();
        System.out.println(userRoles.keySet());

    }
}